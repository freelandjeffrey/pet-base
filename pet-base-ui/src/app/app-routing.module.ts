import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  {
    path: 'home', component: DashboardComponent,
  },
  {
    path: '', pathMatch: 'full', redirectTo: 'home',
  },
  {
    path: 'customers', loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule),
  },
  {
    path: '**', redirectTo: 'home',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
