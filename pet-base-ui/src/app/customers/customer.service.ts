import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pet } from '../pets/entities/pet';
import { Customer } from './entities/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private customer_api_url: string = `${environment.API_BASE}/customers`;

  constructor(private http: HttpClient) { }

  public addCustomer(customer: Customer) {
    return this.http.post<Customer>(this.customer_api_url, customer);
  }

  public getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customer_api_url);
  }

  public getCustomer(id: number):Observable<Customer> {
    return this.http.get<Customer>(`${this.customer_api_url}/${id}`);
  }

  public updateCustomer(id: number, customer: Customer) {
    return this.http.put<Customer>(`${this.customer_api_url}/${id}`, customer);
  }

  public deleteCustomer(customer: Customer) {
    return this.http.delete<Customer>(`${this.customer_api_url}/${customer.id}`);
  }

  public addNewPetToCustomer(customerId: number, newPet: Pet) {
    return this.http.post<Customer>(`${this.customer_api_url}/${customerId}/pets`, newPet);
  }
}
