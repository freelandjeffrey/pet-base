import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.scss']
})
export class CustomerCreateComponent implements OnInit {

  customerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      emailAddress: [''],
    });
  }

  onSubmit(){
    this.customerService.addCustomer(this.customerForm.value).subscribe(
      createdCustomer => this.onSuccess(createdCustomer),
      error => this.onError(error),
    );
  }

  private onSuccess(createdCustomer) {
    console.log(createdCustomer);
    this.router.navigate([`/customers/${createdCustomer.id}`]);
  }

  private onError(error){
    console.log("something went wrong");
    console.log(error);
  }

}
