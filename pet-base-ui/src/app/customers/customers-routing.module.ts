import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerCreateComponent } from './customer-create/customer-create.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerUpdateComponent } from './customer-update/customer-update.component';
import { CustomerViewComponent } from './customer-view/customer-view.component';


const routes: Routes = [
  {
    path: 'list', component: CustomerListComponent,
  },
  {
    path: 'create', component: CustomerCreateComponent,
  },
  {
    path: ':customerId', component: CustomerViewComponent,
  },
  {
    path: ':customerId/edit', component: CustomerUpdateComponent,
  },
  {
    path: '', pathMatch: 'full', redirectTo: 'list',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
