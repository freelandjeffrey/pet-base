import { Pet } from "src/app/pets/entities/pet";

export interface Customer {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    phoneNumber: string;
    emailAddress: string;
    pets: Pet[];
}
