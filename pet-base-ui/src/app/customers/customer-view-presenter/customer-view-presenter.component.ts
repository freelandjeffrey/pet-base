import { Component, Input, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Customer } from '../entities/customer';

@Component({
  selector: 'app-customer-view-presenter',
  templateUrl: './customer-view-presenter.component.html',
  styleUrls: ['./customer-view-presenter.component.scss']
})
export class CustomerViewPresenterComponent implements OnInit {

  @Input() customer: Customer;

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
  }

}
