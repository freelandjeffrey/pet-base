import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerViewPresenterComponent } from './customer-view-presenter.component';

describe('CustomerViewPresenterComponent', () => {
  let component: CustomerViewPresenterComponent;
  let fixture: ComponentFixture<CustomerViewPresenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerViewPresenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerViewPresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.customer = {
      id: 1,
      firstName: 'Test',
      middleName: 'Ing', 
      lastName: 'Person',
      phoneNumber: '0123456789',
      emailAddress: 'test@example.com',
      pets: null
    }
    expect(component).toBeTruthy();
  });
});
