import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerListComponent } from './customer-list.component';

describe('CustomerListComponent', () => {
  let component: CustomerListComponent;
  let fixture: ComponentFixture<CustomerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerListComponent ],
      imports: [HttpClientTestingModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a link to the create page', () => {
    const compiled = fixture.debugElement.nativeElement;
    const createLink = compiled.querySelector('a');
    const CREATE_CUSTOMER_URL: string = '../create'
    expect(createLink.textContent).toBe("Create New");
    expect(createLink.getAttribute("routerLink")).toBe(CREATE_CUSTOMER_URL);
  });

});
