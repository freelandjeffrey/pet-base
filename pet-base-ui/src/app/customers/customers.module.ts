import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerCreateComponent } from './customer-create/customer-create.component';
import { CustomerViewComponent } from './customer-view/customer-view.component';
import { CustomerViewPresenterComponent } from './customer-view-presenter/customer-view-presenter.component';
import { CustomerUpdateComponent } from './customer-update/customer-update.component';
import { PetsModule } from '../pets/pets.module';


@NgModule({
  declarations: [
    CustomerListComponent, 
    CustomerCreateComponent,
    CustomerViewComponent,
    CustomerViewPresenterComponent,
    CustomerUpdateComponent, 
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    PetsModule,
  ]
})
export class CustomersModule { }
