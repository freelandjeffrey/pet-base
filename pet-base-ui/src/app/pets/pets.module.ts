import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetsRoutingModule } from './pets-routing.module';
import { PetListComponent } from './pet-list/pet-list.component';


@NgModule({
  declarations: [PetListComponent],
  imports: [
    CommonModule,
    PetsRoutingModule
  ],
  exports: [PetListComponent],
})
export class PetsModule { }
