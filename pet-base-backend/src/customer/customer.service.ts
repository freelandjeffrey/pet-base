import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatePetDto } from 'src/pet/dto/create-pet.dto';
import { Pet } from 'src/pet/entities/pet.entity';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomerService {

  constructor(
      @InjectRepository(Customer) private customerRepository: Repository<Customer>,
      @InjectRepository(Pet) private petRepository: Repository<Pet>
    ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customerRepository.find();
  }

  findOne(id: number) {
    return this.customerRepository.findOne(id);
  }

  update(id: number, updateCustomerDto: UpdateCustomerDto) {
    return this.customerRepository.update(id, updateCustomerDto);
  }

  remove(id: number) {
    return this.customerRepository.delete(id);
  }

  async addNewPetToCustomer(customerId: number, createPetDto: CreatePetDto) {
    const customer = await this.customerRepository.findOne(customerId)
    const newPet = this.petRepository.create(createPetDto);
    customer.pets.push(newPet);
    return this.customerRepository.save(customer);
  }

}
