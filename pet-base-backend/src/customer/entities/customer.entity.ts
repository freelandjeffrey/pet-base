import { Pet } from "src/pet/entities/pet.entity";
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Customer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column({
        nullable: true,
    })
    middleName: string;

    @Column()
    lastName: string;

    @Column({
        nullable: true,
    })
    phoneNumber: string;

    @Column({
        nullable: true,
    })
    emailAddress: string;

    @ManyToMany(type => Pet, (pet) => pet.owners, {
        eager: true,
        cascade: true
    })
    @JoinTable()
    pets: Pet[];
}
