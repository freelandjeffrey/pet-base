import { Module } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { CustomerController } from './customer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { PetModule } from 'src/pet/pet.module';
import { Pet } from 'src/pet/entities/pet.entity';

@Module({
  controllers: [CustomerController],
  imports: [
    TypeOrmModule.forFeature([Customer, Pet]),
    PetModule,
  ],
  providers: [CustomerService]
})
export class CustomerModule {}
