import { Controller, Get, Post, Body, Put, Param, Delete, Patch } from '@nestjs/common';
import { CreatePetDto } from 'src/pet/dto/create-pet.dto';
import { CustomerService } from './customer.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';

@Controller('customers')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Post()
  create(@Body() createCustomerDto: CreateCustomerDto) {
    return this.customerService.create(createCustomerDto);
  }

  @Get()
  findAll() {
    return this.customerService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.customerService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCustomerDto: UpdateCustomerDto) {
    return this.customerService.update(+id, updateCustomerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.customerService.remove(+id);
  }

  @Get(':id/pets')
  findCustomerPets(@Param('id') id: string) {
    return this.customerService.findOne(+id).then(customer => customer.pets || []);
  }

  @Post(':id/pets')
  addNewPetToCustomer(@Param('id') id: string, @Body() createPetDto: CreatePetDto) {
    return this.customerService.addNewPetToCustomer(+id, createPetDto);
  }

}
