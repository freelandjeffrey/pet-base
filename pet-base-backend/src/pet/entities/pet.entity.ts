import { Customer } from "src/customer/entities/customer.entity";
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Pet {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(type => Customer, (customer) => customer.pets)
    owners: Customer[];
}
